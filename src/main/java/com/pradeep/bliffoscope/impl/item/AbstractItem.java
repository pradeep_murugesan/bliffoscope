package com.pradeep.bliffoscope.impl.item;

import com.pradeep.bliffoscope.model.Point;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractItem {
    private boolean[][] image;

    private int width;
    private int height;

    public  AbstractItem(String fileName) {
        this.height = this.width = getGridLength(fileName);
        this.image = new boolean[height][width];
        populateImageGrid(fileName);

    }

    public abstract String getFileName();

    public abstract String getName();

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean[][] getImage() {
        return this.image;
    }
    protected InputStream getInputStream(String filename) {
        InputStream inputStream = this.getClass().getResourceAsStream("/" + filename);
        if (inputStream == null) {
            try {
                throw new FileNotFoundException(filename);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return new BufferedInputStream(inputStream);
    }

    protected int getGridLength(String filename) {
        InputStream is = new BufferedInputStream(getInputStream(filename));
        int count = 0;
        int readChars = 0;
        int maxCoulmns = 0;
        try {
            byte[] c = new byte[1024];

            boolean endsWithoutNewLine = false;
            while ((readChars = is.read(c)) != -1) {
                for (int i = 0, j=0; i < readChars; ++i, ++j) {
                    if (c[i] == '\n') {
                        ++count;
                        if (maxCoulmns < j) {
                            maxCoulmns = j;
                        }
                        j = 0;
                    }
                }
                endsWithoutNewLine = (c[readChars - 1] != '\n');
            }
            if(endsWithoutNewLine) {
                ++count;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("The width returned is max of " + count + " and " + maxCoulmns);
        return Math.max(count, maxCoulmns);
    }

    protected void populateImageGrid(String filename) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(getInputStream(filename)));
            String line;
            int col;
            int row = 0;
            while((line = reader.readLine()) != null && row < this.getHeight()) {
                for(col = 0; col < line.length() && col < this.getWidth(); col++) {
                    this.image[row][col] = line.charAt(col) == '+';
                }
                row++;
            }
            reader.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }

    // TODO: to be removed
    @Override
    public String toString() {
        StringBuilder pattern = new StringBuilder();
        for(int row = 0; row < this.getHeight() ; row++) {
            for(int col = 0; col < this.getWidth(); col++) {
                pattern.append(this.image[row][col] ? '+' : ' ');
            }
            pattern.append(System.getProperty("line.separator"));
        }
        return pattern.toString();
    }
}
