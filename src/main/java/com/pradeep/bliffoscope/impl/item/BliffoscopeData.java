package com.pradeep.bliffoscope.impl.item;

public class BliffoscopeData extends AbstractItem {

    private String fileName;

    public BliffoscopeData(String fileName) {
        super(fileName);
        this.fileName = fileName;
    }

    @Override
    public String getFileName() {
        return this.fileName;
    }

    @Override
    public String getName() {
        return null;
    }


}
