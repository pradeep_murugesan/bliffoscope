package com.pradeep.bliffoscope.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;


@ApiModel(description = "")
public class SpaceItem  {

    private String name = null;
    private HashMap<Integer, List<Point>> coordinates;
    private Integer bestMatch;
    private Integer minMatch;

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("coordinates")
    public HashMap<Integer, List<Point>> getCoordinates() {
        return coordinates;
    }

    @ApiModelProperty(value = "")
    @JsonProperty("bestMatch")
    public Integer getBestMatch() {
        return bestMatch;
    }

    @ApiModelProperty(value = "")
    @JsonProperty("minMatch")
    public Integer getMinMatch() {
        return minMatch;
    }

    public void setCoordinates(HashMap<Integer, List<Point>> coordinates) {
        this.coordinates = coordinates;
    }

    public void setBestMatch(Integer bestMatch) {
        this.bestMatch = bestMatch;
    }

    public void setMinMatch(Integer minMatch) {
        this.minMatch = minMatch;
    }
}

