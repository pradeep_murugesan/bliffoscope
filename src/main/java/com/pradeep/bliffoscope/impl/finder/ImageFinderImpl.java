package com.pradeep.bliffoscope.impl.finder;

import static com.pradeep.bliffoscope.utils.BliffoscopeUtils.calculatePoints;
import static com.pradeep.bliffoscope.utils.BliffoscopeUtils.getMatchPercentagePoints;
import static com.pradeep.bliffoscope.utils.BliffoscopeUtils.getUniqueScores;
import com.pradeep.bliffoscope.impl.FinderConfiguration;
import com.pradeep.bliffoscope.impl.item.AbstractItem;
import com.pradeep.bliffoscope.model.Point;
import com.pradeep.bliffoscope.model.SpaceItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

public class ImageFinderImpl implements ImageFinder {

    private FinderConfiguration configuration;
    private ArrayList<Point> correctCoordinates;

    public ImageFinderImpl(FinderConfiguration configuration) {
        this.configuration = configuration;
        this.correctCoordinates = new ArrayList<Point>();
    }

    @Override
    public SpaceItem findItem(AbstractItem item, AbstractItem data) {
        int area = item.getHeight() * item.getWidth();
        boolean[][] itemImage = item.getImage();
        boolean[][] dataImage = data.getImage();
        int[][] scores = calculatePoints(itemImage, dataImage);
        SortedSet<Integer> set = getUniqueScores(scores)
                .headSet(getMatchPercentagePoints(area, this.configuration.getMatchPercentage()));
        HashMap<Integer, List<Point>>  coordinates = this.getMatchingCoordinates(set, scores);
        HashMap<Integer, List<Point>>  rightCoordinates = this.removeFalsePositives(coordinates);
        return getSpaceItem(rightCoordinates, item);
    }

    private  SpaceItem getSpaceItem(HashMap<Integer, List<Point>> rightCoordinates, AbstractItem item) {
        int area = item.getHeight() * item.getWidth();
        SpaceItem spaceItem = new SpaceItem();
        spaceItem.setBestMatch(area);
        spaceItem.setMinMatch(getMatchPercentagePoints(area, this.configuration.getMatchPercentage()));
        spaceItem.setName(item.getName());
        spaceItem.setCoordinates(rightCoordinates);
        return spaceItem;
    }

    private HashMap<Integer, List<Point>> removeFalsePositives(HashMap<Integer, List<Point>>  coordinates) {
        HashMap<Integer, List<Point>> rightPoints = new HashMap<Integer, List<Point>>();

        for(Map.Entry<Integer, List<Point>> entry : coordinates.entrySet()) {
            List<Point> points = entry.getValue();
            List<Point> rightCoordinates = new ArrayList<Point>();
            Integer key = entry.getKey();
            for(Point point : points) {
                if(this.correctCoordinates.contains(point)) {
                    rightCoordinates.add(point);
                }
            }
            if(!rightCoordinates.isEmpty()) {
                rightPoints.put(key, rightCoordinates);
            }
        }
        return rightPoints;
    }


    private HashMap<Integer, List<Point>> getMatchingCoordinates(SortedSet<Integer> set, int[][] points) {
        HashMap<Integer, List<Point>> map = new HashMap<Integer, List<Point>>();

        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            Integer value = iterator.next();
            List<Point> coordinates = new ArrayList<Point>();
            for(int row = 0; row < points.length; row++) {
                for (int col = 0; col < points[row].length; col++) {
                    if(points[row][col] == value) {
                        Point coordinate = new Point(row, col);
                        coordinates.add(coordinate);
                        addToCorrectCoordinates(coordinate);
                    }
                }
            }
            map.put(value, coordinates);
        }
        return map;
    }

    private void addToCorrectCoordinates(Point coordinate) {
        boolean flag = false;
        for(Point point : correctCoordinates) {
            if(point.liesInFalsePositiveRange( coordinate, this.configuration.getFalsePositiveRange() ) ) {
                flag = true;
            }
        }
        if(!flag) {
            this.correctCoordinates.add(coordinate);
        }
    }
}
