package com.pradeep.bliffoscope.impl;

public class FinderConfiguration {
    private int matchPercentage;
    private int falsePositiveRange;

    public FinderConfiguration(Integer matchPercentage, int falsePositiveRange) {
        this.matchPercentage = matchPercentage;
        this.falsePositiveRange = falsePositiveRange;
    }

    public int getMatchPercentage() {
        return matchPercentage;
    }

    public void setMatchPercentage(int matchPercentage) {
        this.matchPercentage = matchPercentage;
    }

    public int getFalsePositiveRange() {
        return falsePositiveRange;
    }

    public void setFalsePositiveRange(int falsePositiveRange) {
        this.falsePositiveRange = falsePositiveRange;
    }
}
