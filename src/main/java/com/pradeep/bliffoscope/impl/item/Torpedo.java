package com.pradeep.bliffoscope.impl.item;

public class Torpedo extends AbstractItem {

    public static final String fileName = "SlimeTorpedo.txt";
    private String name = "SlimeTorpedo";
    public Torpedo() {
        super(fileName);
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getName() {
        return name;
    }
}
