package com.pradeep.bliffoscope.api;

import com.pradeep.bliffoscope.exception.NotFoundException;
import com.pradeep.bliffoscope.impl.FinderConfiguration;
import com.pradeep.bliffoscope.impl.finder.ImageFinderImpl;
import com.pradeep.bliffoscope.impl.item.AbstractItem;
import com.pradeep.bliffoscope.impl.item.BliffoscopeData;
import com.pradeep.bliffoscope.impl.item.Starship;
import com.pradeep.bliffoscope.impl.item.Torpedo;
import com.pradeep.bliffoscope.model.Point;
import com.pradeep.bliffoscope.model.SpaceItem;

import java.math.BigDecimal;

import io.swagger.annotations.*;

import io.swagger.models.auth.In;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.springframework.http.MediaType.*;

@Controller
@RequestMapping(value = "/find", produces = {APPLICATION_JSON_VALUE})
@Api(value = "/find", description = "find the space item(s) in the image")
public class FindApi {

  @ApiOperation(value = "", notes = "Gets the location of all kind of objects. Optional query param of **matchPercentage** determines the % of match that is used. Default value is 75. Also the false positve range can be supplied as query param. default value is 10", response = SpaceItem.class, responseContainer = "List")
  @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Successful response", response = SpaceItem.class) })
  @RequestMapping(value = "/all",


    method = RequestMethod.GET)
  public ResponseEntity<List<SpaceItem>> findAllGet(
          @ApiParam(value = "Percentage of the match") @RequestParam(value = "matchPercentage", required = false) Integer matchPercentage,
          @ApiParam(value = "falsePositiveRange of the match") @RequestParam(value = "falsePositiveRange", required = false) Integer falsePositiveRange
  ) throws NotFoundException {
      // do some magic!
      if(matchPercentage == null) {
          matchPercentage = 75;
      }
      if(falsePositiveRange == null) {
          falsePositiveRange = 10;
      }
      FinderConfiguration finderConfiguration = new FinderConfiguration(matchPercentage, falsePositiveRange);
      ImageFinderImpl imageFinder = new ImageFinderImpl(finderConfiguration);
      AbstractItem torpedo = new Torpedo();
      AbstractItem starShip = new Starship();
      AbstractItem data = new BliffoscopeData("TestData.txt");
      SpaceItem torpedo_results = imageFinder.findItem(torpedo, data);
      SpaceItem starship_results = imageFinder.findItem(starShip, data);
      List<SpaceItem> spaceItems = new ArrayList<SpaceItem>();
      spaceItems.add(torpedo_results);
      spaceItems.add(starship_results);
      return new ResponseEntity<List<SpaceItem>>(spaceItems, HttpStatus.OK);
  }

}
