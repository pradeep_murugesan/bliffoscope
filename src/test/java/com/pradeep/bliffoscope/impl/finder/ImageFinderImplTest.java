package com.pradeep.bliffoscope.impl.finder;

import com.pradeep.bliffoscope.impl.FinderConfiguration;
import com.pradeep.bliffoscope.impl.item.AbstractItem;
import com.pradeep.bliffoscope.impl.item.BliffoscopeData;
import com.pradeep.bliffoscope.impl.item.Torpedo;
import com.pradeep.bliffoscope.model.SpaceItem;
import org.junit.Assert;
import org.junit.Test;

public class ImageFinderImplTest {
    @Test
    public void testFinder() {
        AbstractItem torpedo = new Torpedo();
        AbstractItem data = new BliffoscopeData("TestData.txt");
        FinderConfiguration finderConfiguration = new FinderConfiguration(75, 10);
        finderConfiguration.setFalsePositiveRange(10);
        ImageFinder imageFinder = new ImageFinderImpl(finderConfiguration);
        SpaceItem item = imageFinder.findItem(torpedo, data);
        Assert.assertTrue(item.getBestMatch() == 169);
        Assert.assertTrue(item.getMinMatch() == 127);
        Assert.assertEquals(item.getName(), "SlimeTorpedo");
        Assert.assertEquals(item.getCoordinates().size(), 1);
    }
}
