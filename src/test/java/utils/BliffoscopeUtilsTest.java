package utils;

import static com.pradeep.bliffoscope.utils.BliffoscopeUtils.isInRange;

import com.pradeep.bliffoscope.impl.item.AbstractItem;
import com.pradeep.bliffoscope.impl.item.BliffoscopeData;
import com.pradeep.bliffoscope.impl.item.Torpedo;
import com.pradeep.bliffoscope.utils.BliffoscopeUtils;
import org.junit.Assert;
import org.junit.Test;

public class BliffoscopeUtilsTest {

    @Test
    public void getMatchPercentagePointsTest() {
        int minPercent = BliffoscopeUtils.getMatchPercentagePoints(100, 50);
        Assert.assertTrue(minPercent == 50);
    }


    @Test
    public void isInRangeTest() {
        int a = 5, b = 10, range = 7 ;
        Assert.assertTrue(isInRange(a, b, range));
    }

    @Test
    public void isInRangeFalseTest() {
        int a = 5, b = 13, range = 7 ;
        Assert.assertFalse(isInRange(a, b, range));
    }

    @Test
    public void testCalculatePoints() {
        AbstractItem torpedo = new Torpedo();
        AbstractItem data = new BliffoscopeData("TestData.txt");
        int[][] points = BliffoscopeUtils.calculatePoints(torpedo.getImage(), data.getImage());
        Assert.assertTrue(points.length == 101);
    }
}
