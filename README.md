Blifoscope
===========

This solution uses Java and Spring Boot. Composed the solution as a rest service. 

#### Implementation

The solution scans the 100x100 data and checks for the spaceitems.

It calculates the % match for the space item starting with each cell. We calculate the point for the cell. The cell will get a point if it matches with the space item, else it will not.

So for the 13x13 Torpedo the best match in the data would be 169. The solution checks for a percentage of 75 which is default. The percentage can be change through the url param.

We also eradicate the duplicate that falls with the range of 10 cells which is also configurable.

#### Build and run

Checkout the source and execute the following command

```
mvn spring-boot:run

```

the server will start in the port 8080. Access the following URL

```
http://localhost:8080/
```
To get the swagger documentation for the api.

You can access the api @ 

```
curl -X GET "http://localhost:8080/find/all"
```

#### Deployment

The version of the application is also deployed in heroku. You can access the swagger ui for the deployed version [here](https://bliffoscope.herokuapp.com/swagger-ui.html#!/find-api/findAllGetUsingGET)

```
https://bliffoscope.herokuapp.com/
```