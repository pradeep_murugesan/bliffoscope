package com.pradeep.bliffoscope.impl.item;

public class Starship extends AbstractItem{

    public static final String fileName = "Starship.txt";
    private String name = "StarShip";
    public Starship() {
        super(fileName);
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getName() {
        return name;
    }
}
