package com.pradeep.bliffoscope.impl.item;

import org.junit.Assert;
import org.junit.Test;

public class AbstractItemTest {

    @Test
    public void getGridLengthTest() {
        Torpedo torpedo = new Torpedo();
        int gridLength = torpedo.getGridLength(torpedo.getFileName());
        System.out.println(gridLength);
        Assert.assertEquals(13, gridLength);
    }

    @Test
    public void printPattern() {
        Torpedo torpedo = new Torpedo();
        System.out.println(torpedo.toString());
    }
}
