package com.pradeep.bliffoscope.model;

import static com.pradeep.bliffoscope.utils.BliffoscopeUtils.isInRange;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;


@ApiModel(description = "")
public class Point  {

    private Integer x = null;
    private Integer y = null;

    public Point(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
    }

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("x")
    public Integer getX() {
        return x;
    }
    public void setX(Integer x) {
        this.x = x;
    }

    /**
     **/
    @ApiModelProperty(value = "")
    @JsonProperty("y")
    public Integer getY() {
        return y;
    }
    public void setY(Integer y) {
        this.y = y;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Point point = (Point) o;
        return Objects.equals(x, point.x) &&
                Objects.equals(y, point.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public boolean liesInFalsePositiveRange(Point point, int range) {
        boolean falsePositive = false;
        boolean xRange = isInRange(this.getX(), point.getX(), range);
        boolean yRange = isInRange(this.getY(), point.getY(), range);
        if( xRange && yRange) {
            falsePositive = true;
        }
        return falsePositive;
    }




    @Override
    public String toString()  {
        StringBuilder sb = new StringBuilder();
        sb.append("class Point {\n");

        sb.append("  x: ").append(x).append("\n");
        sb.append("  y: ").append(y).append("\n");
        sb.append("}\n");
        return sb.toString();
    }
}
