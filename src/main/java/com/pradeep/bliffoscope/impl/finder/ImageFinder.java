package com.pradeep.bliffoscope.impl.finder;

import com.pradeep.bliffoscope.impl.item.AbstractItem;
import com.pradeep.bliffoscope.model.SpaceItem;

public interface ImageFinder {

    public SpaceItem findItem(AbstractItem item, AbstractItem data);
}
