package com.pradeep.bliffoscope.utils;

import java.util.NavigableSet;
import java.util.TreeSet;

public class BliffoscopeUtils {

    public static Integer getMatchPercentagePoints(int area, int matchPercentage) {
        Integer minPercent = Math.round(((float)(area * matchPercentage) / 100 ));
        System.out.println(matchPercentage + " % of the area " + area +" is " + minPercent);
        return minPercent;
    }

    public static boolean isInRange(int a , int b, int range) {
        boolean liesInRange = false;
        if(a + range > b && a - range < b) {
            liesInRange = true;
        }
        return liesInRange;
    }

    public static NavigableSet<Integer> getUniqueScores(int[][] points) {
        TreeSet<Integer> set = new TreeSet<Integer>();
        for(int row = 0; row < points.length; row++) {
            for (int col = 0; col < points[row].length; col++) {
                set.add(points[row][col]);
            }
        }
        return set.descendingSet();
    }

    public static int[][] calculatePoints(boolean[][] itemImage, boolean[][] dataImage) {
        int [][] points = new int[dataImage.length][dataImage.length];
        for(int row = 0; row < dataImage.length ; row++) {
            for(int col = 0; col < dataImage[row].length; col++) {
                int point = 0;
                for(int itemRow = 0; itemRow < itemImage.length ; itemRow++) {
                    for(int itemCol = 0; itemCol < itemImage[itemRow].length
                            && (itemRow + row < dataImage.length) &&
                            (itemCol + col < dataImage[itemRow].length); itemCol++) {

                        if(! itemImage[itemRow][itemCol] ^
                                dataImage[row + itemRow][col + itemCol]) {

                            point++;
                        }
                    }
                }
                points[row][col] = point;
            }
        }
        return points;
    }
}
