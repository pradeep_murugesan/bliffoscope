package com.pradeep.bliffoscope.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class PointTest {

    public static Point a;
    @BeforeClass
    public static void setUp() {
        a = new Point();
        a.setY(10);
        a.setX(10);
    }

    @Test
    public void testLiesInFalsePositiveRange() {
        Point b = new Point();
        b.setY(12);
        b.setX(12);
        Assert.assertTrue(a.liesInFalsePositiveRange(b, 10));
    }

    @Test
    public void testLiesInFalsePositiveRangeFalse() {
        Point b = new Point();
        b.setY(22);
        b.setX(22);
        Assert.assertFalse(a.liesInFalsePositiveRange(b, 10));
    }
}
